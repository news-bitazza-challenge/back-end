import Joi from '@hapi/joi'


export const addNews = Joi.object().keys({
  title: Joi.string().required(),
  description: Joi.string().required(),
  image: Joi.string().required(),
})

export const patchNews = Joi.object().keys({
  title: Joi.string(),
  description: Joi.string(),
  publish: Joi.date(),
  image: Joi.string(),
})

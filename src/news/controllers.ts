import Koa from 'koa'
import { Op } from 'sequelize'
import { isLogin, body } from '../common/utils/koa-decorators'
import dateOnly from '../common/utils/date-only'
import * as schema from './validator'
import { News } from './models'


export class Controller {
  
  public async publicNews(ctx: Koa.Context, next: () => Promise<any>) {
    const dateNow = dateOnly()
    dateNow.setUTCDate(dateNow.getUTCDate() + 1)
    const dateLast7Days = new Date(dateNow)
    dateLast7Days.setUTCDate(dateLast7Days.getUTCDate() -7)
    console.log(dateLast7Days, dateNow)
    const listNews = await News.findAll({
      where: {
        publish: {
          [Op.between]: [dateLast7Days, dateNow],
        },
      },
      order: [['publish', 'DESC']],
    })
    ctx.body = listNews
    return next()
  }

  public async publicViewNews(ctx: Koa.Context, next: () => Promise<any>) {
    const news = await News.findByPk(ctx.params.newsSlug)
    ctx.assert(news, 404, `not found news: ${ctx.params.newsSlug}`)
    ctx.body = news
    return next()
  }

  @isLogin()
  public async news(ctx: Koa.Context, next: () => Promise<any>) {
    const where: any = {}
    if(ctx.query.date) {
      const start = dateOnly(ctx.query.date)
      const end = new Date(start)
      end.setUTCDate(start.getUTCDate() + 1)
      where.publish = {
        [Op.between]: [ start, end ],
      }
    }
    const listNews = await News.findAll({
      where,
      order: [['publish', 'DESC']],
    })
    ctx.body = listNews
    return next()
  }

  @isLogin()
  @body(schema.addNews)
  public async addNews(ctx: Koa.Context, next: () => Promise<any>) {
    const { title, description, image } = ctx.request.body
    const news = News.build()
    news.slug = await News.slugify(title)
    news.title = title
    news.description = description
    news.image = image
    news.save()
    ctx.body = news
    ctx.starus = 201
    return next()
  }

  @isLogin()
  public async viewNews(ctx: Koa.Context, next: () => Promise<any>) {
    const news = await News.findByPk(ctx.params.newsSlug)
    ctx.assert(news, 404, `not found news: ${ctx.params.newsSlug}`)
    ctx.body = news
    return next()
  }

  @isLogin()
  @body(schema.patchNews)
  public async updateNews(ctx: Koa.Context, next: () => Promise<any>) {
    const news = await News.findByPk(ctx.params.newsSlug)
    ctx.assert(news, 404, `not found news: ${ctx.params.newsSlug}`)
    const { title, description, publish, image } = ctx.request.body
    news.title = title || news.title
    news.description = description || news.description
    news.image = image || news.image
    news.publish = publish || news.publish
    news.save()
    ctx.body = news
    return next()
  }

  @isLogin()
  public async deleteNews(ctx: Koa.Context, next: () => Promise<any>) {
    const news = await News.findByPk(ctx.params.newsSlug)
    ctx.assert(news, 404, `not found news: ${ctx.params.newsSlug}`)
    await news.destroy()
    ctx.body = `successfully delete news: ${ctx.params.newsSlug}`
    return next()
  }

}

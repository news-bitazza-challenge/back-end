import Router from 'koa-router'
import { Controller } from './controllers'

const router = new Router()
const controller = new Controller()

router.get('/public/news', controller.publicNews)
router.get('/public/news/:newsSlug', controller.publicViewNews)

router.get('/news', controller.news)
router.post('/news', controller.addNews)
router.get('/news/:newsSlug', controller.viewNews)
router.patch('/news/:newsSlug', controller.updateNews)
router.delete('/news/:newsSlug', controller.deleteNews)

export default router

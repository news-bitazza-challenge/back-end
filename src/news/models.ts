import { Model, DataTypes } from 'sequelize'
import slugify from 'slugify'
import sequelize from '../common/utils/connection'


class News extends Model {
  public slug!: string
  public title!: string
  public description!: string
  public publish!: Date
  public image!: string

  static async slugify(title: string) {
    const checkExistSlug = async (slug, fill=0) => {
      const checkSlug = `${slug}${fill || ''}` 
      const findSlug = await this.findByPk(checkSlug)
      if (findSlug) {
        const useSlug = await checkExistSlug(slug, fill+1)
        return useSlug
      }
      return checkSlug
    }
    const slug = await checkExistSlug(slugify(title, '-'))
    return slug
  }
}
News.init({
  slug: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
    primaryKey: true,
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  publish: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: Date.now,
  },
  image: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  sequelize,
  freezeTableName: true,
  tableName: 'news_news',
  timestamps: false,
})

export { News }

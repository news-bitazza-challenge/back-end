'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const bcrypt = require('bcryptjs')
    const faker = require('faker')
    const slugify =  require('slugify')
    let transaction
    transaction = await queryInterface.sequelize.transaction()
    await queryInterface.bulkInsert(
      'common_auth',
      [{
        username: 'admin',
        password: bcrypt.hashSync('123456', bcrypt.genSaltSync()),
        refresh_token: JSON.stringify([]),
        created_at: new Date(),
      }],
      { transaction },
    )
    await transaction.commit()

    const newsImage = [
      'https://static.foxnews.com/static/orion/styles/img/fox-news/og/og-fox-news.png',
      'https://media4.s-nbcnews.com/j/newscms/2019_01/2705191/nbc-social-default_b6fa4fef0d31ca7e8bc7ff6d117ca9f4.nbcnews-fp-1200-630.png',
      'http://www.bbc.co.uk/news/special/2015/newsspec_10857/bbc_news_logo.png?cb=1',
      'https://nodeassets.nbcnews.com/cdnassets/projects/socialshareimages/og-nbcnews1200x630.png',
      'https://pbs.twimg.com/media/Dreb-jsXgAA3J2t.jpg',
      'https://s.abcnews.com/images/US/190701_vod_live_headlines_hpMain_16x9_992.jpg',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoYilgiIConGAB-SJG76ckTJjy5zG5XUvhXuEj1YqBFzEmpm_W',
      'https://e3.365dm.com/19/03/768x432/skynews-divided-caolan-robertson_4600321.jpg?20190307144603',
      'https://www.bnd.com/latest-news/qd0h5p/picture222958020/alternates/LANDSCAPE_768/NEWSnew.jpg',
    ]
    const newsMock = []
    for (let i = 0; i < 50; i++) {
      const title = faker.lorem.words()
      const dateNow = new Date()
      dateNow.setMinutes(dateNow.getMinutes() - faker.random.number(30000))
      newsMock.push({
        slug: slugify(title, '-'),
        title: title,
        description: faker.lorem.paragraphs(),
        publish: dateNow,
        image: newsImage[faker.random.number(8)],
      })
    }
    transaction = await queryInterface.sequelize.transaction()
    await queryInterface.bulkInsert('news_news', newsMock, { transaction })
    await transaction.commit()
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};

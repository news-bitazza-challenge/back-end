

export default (date?) => {
	const _date = date ? new Date(date) : new Date()
	return new Date(
		_date.getUTCFullYear(),
		_date.getUTCMonth(),
		_date.getUTCDate()
	)
}
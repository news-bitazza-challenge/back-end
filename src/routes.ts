import Router from 'koa-router'
import commonRouter from './common/routes'
import newsRouter from './news/routes'
import conf from './config'


const rootRouter = new Router({ prefix: `/${conf.apiPrefix}` })

rootRouter.use('/common', commonRouter.routes(), commonRouter.allowedMethods())
rootRouter.use('/news', newsRouter.routes(), newsRouter.allowedMethods())

export default rootRouter

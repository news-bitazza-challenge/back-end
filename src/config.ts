
export interface IConfig {
  port: number
  apiPrefix: string
  debug: boolean
  jwtSecret: string
  issuer: string
  databaseUrl: string
  mediaPath: string
  mediaURL: string
}

const config: IConfig = {
  port: +process.env.PORT || 3000,
  apiPrefix: 'api',
  debug: process.env.NODE_ENV !== 'production',
  jwtSecret: process.env.JWT_SECRET || 'your-secret-whatever',
  issuer: 'bitazza',
  databaseUrl: process.env.DATABASE_URL || 'mysql://bitazza:bitazza@db.bitazza:3306/bitazza',
  mediaPath: process.env.MEDIA_PATH || '/data/media',
  mediaURL: process.env.MEDIA_URL || '/media',
}

export default config
